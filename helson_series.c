#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int factorcounter;
int twocounter;
unsigned long long x_rep;

int numprime (unsigned long long factoring) {
	unsigned long long factor_step_counter = 0;
	for (int i = 2; (i <= factoring); i++) {
		while (1) {
			if ((factoring % i) == 0) {
				factor_step_counter++;
				factoring = (factoring / i);
			}
			else break;
		}
	}
	return factor_step_counter;
}

int main(int argc, char** argv) {
        unsigned long long max_rep = (strtol (argv[1], NULL, 10));
	factorcounter = 0;
	twocounter = 0;
	for (unsigned long long i = 0; i <= max_rep; i++) {
		if (numprime (i) == 2) twocounter++;
		printf ("%d,%d\n", i, twocounter);
	}

	return 0;
}
