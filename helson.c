#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int factorcounter;
int twocounter;

// int prime(int x) {
// 	if (x < 2) return 0;
// 	for(int i=2; i <= sqrt(x); i++)
// 		if ((x % i) == 0) return 0;
// 	return 1;
// }

int numprime (int factoring) {
	int factor_step_counter = 0;
	printf ("%s%d\n", "now factoring:", factoring);
	for (int i = 2; (i <= factoring); i++) {
		while (1) {
			if ((factoring % i) == 0) {
				factor_step_counter++;
				factoring = (factoring / i);
				printf ("found: %d\n", i);
			}
			else break;
		}
	}
	printf ("number found : %d\n", factor_step_counter);
	return factor_step_counter;
}

int main(int argc, char** argv) {
	factorcounter = 0;
	twocounter = 0;
	long max_rep = (strtol (argv[1], NULL, 10));
        printf ("%d\n", max_rep);
	for (long i = 0; i <= max_rep; i++)
		if (numprime (i) == 2) twocounter++;

	printf ("result: %d\n", twocounter);
	return 0;
}
