#include <stdlib.h>
#include <stdio.h>
#include <math.h>

unsigned long long factorcounter;
unsigned long long twocounter;
unsigned long long x_rep;

int numprime (unsigned long long factoring) {
        unsigned long long factor_step_counter = 0;
        for (int i = 2; (i <= factoring); i++) {
                while (1) {
                        if ((factoring % i) == 0) {
                                factor_step_counter++;
                                factoring = (factoring / i);
                        }
                        else break;
                }
        }
        return factor_step_counter;
}

int main(int argc, char** argv) {
        unsigned long long max_rep = (strtol (argv[1], NULL, 10));
        factorcounter = 0;
        twocounter = 0;
        for (unsigned long long i = 0; i <= max_rep; i++) {
                if (numprime (i) == 2) twocounter++;
		if (twocounter == 0) printf ("%d,0\n", i);
                else printf ("%d,%d\n", i, ((twocounter * 100000) / i));
        }

        return 0;
}
