#include <stdlib.h>
#include <stdio.h>
#include <math.h>

// a sequence longer than 10 will give a segmentation fault

unsigned long long factorcounter;
unsigned long long twocounter;
unsigned long long max_rep;
unsigned long long orderarray[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0} ;

int numprime (unsigned long long factoring) {
	unsigned long long factor_step_counter = 0;
	for (int i = 2; (i <= factoring); i++) {
		while (1) {
			if ((factoring % i) == 0) {
				factor_step_counter++;
				factoring = (factoring / i);
			}
			else break;
		}
	}
	return factor_step_counter;
}

int main(int argc, char** argv) {
        max_rep = (strtol (argv[1], NULL, 10));
	factorcounter = 0;
	twocounter = 0;
	for (unsigned long long i = 0; i <= max_rep; i++) {
		if (numprime (i) == 2)
			twocounter++;
		else if (twocounter != 0) {
			(orderarray[twocounter - 1])++;
			twocounter = 0;
		}
	}

	for (int i = 0; i < 10; i++)
		printf ("%d:%d\n", i + 1, orderarray[i]);

	return 0;
}
